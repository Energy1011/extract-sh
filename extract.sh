#!/bin/bash
# Original source-code: 
# https://askubuntu.com/questions/526270/ranger-file-manager-getting-it-to-open-archive-files-with-custom-script

# Modified & commented by https://gitlab.com/Energy1011 
# Copyright (C) Energy1011

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#Welcome and GLOBALS
SCRIPTNAME="${0##*/}"
echo "Extract files, this is "$SCRIPTNAME
ARC="$1"
DEFAULT_TARGET="$(pwd)"

# Function to show usage 
usage(){
echo -e "
Usage: $0 [OPTIONS] [ARGUMENTS]
    OPTIONS:
    -h  show this help

    EXAMPLES:
    Example - $0 <compressed_file_name>
        We need to pass an argument <compressed_file_name> compressed file, like 'files.zip'
"
exit 1
}

# Funciton to display errors
err() {
    printf >&2 "$SCRIPTNAME: $*\n"
    exit 1
}

#Main function to extract
extract_file(){
  # Get file path with readlink
  [[ -f $ARC ]] || err $"'$ARC' file does not exist"
  ARC="$(readlink -f "$ARC")"

  # Where we are going to extract ?
  read -p "Extract to [default: $DEFAULT_TARGET]: ?" TARGET
  [[ -z $TARGET ]] && TARGET="$DEFAULT_TARGET"
  [[ -d $TARGET ]] || err $"Directory '$TARGET' does not exist"
  [[ -w $TARGET ]] || err $"Permission denied: '$TARGET' is not writable"

  # Change to TARGET directory to extract files
  cd "$TARGET"

  # Cases for filetype to extract
  case "$ARC" in
      *.tar.bz2)   tar xjf "$ARC"     ;;
      *.tar.gz)    tar xzf "$ARC"     ;;
      *.bz2)       bunzip2 "$ARC"     ;;
      *.rar)       unrar e "$ARC"     ;;
      *.gz)        gunzip "$ARC"      ;;
      *.tar)       tar xvf "$ARC"      ;;
      *.tbz2)      tar xjf "$ARC"     ;;
      *.tgz)       tar xzf "$ARC"     ;;
      *.zip)       unzip "$ARC"       ;;
      *.Z)         uncompress "$ARC"  ;;
      *.7z)        7z x "$ARC"        ;;
      *)           echo "'$ARC' cannot be extracted by $SCRIPTNAME" ;;
  esac
}

  # Here we begin
  # Check for Arg options
  OPTSTRING='*h'
  while getopts $OPTSTRING option; do
          case $option in
                  h) usage; exit;;
                  :) echo "Missing argument for option: -$OPTARG"; usage; >&2; exit 1;;
                  *) echo "Unimplemented option: -$OPTARG"; usage; >&2; exit 1;;
          esac
  done
   # Are there args ?
   if [ -z "$*" ]; then echo "No arguments given..."; usage; fi
   # Default extract file
   extract_file
